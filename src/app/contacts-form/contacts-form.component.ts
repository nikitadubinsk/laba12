import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { People } from '../app.component';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-contacts-form',
  templateUrl: './contacts-form.component.html',
  styleUrls: ['./contacts-form.component.css']
})
export class ContactsFormComponent implements OnInit {

  form : FormGroup;


  @Output() onAdd: EventEmitter<People> = new EventEmitter<People>()

  name = "";
  surname = "";
  telephone = "";
  disabled = true;
  i = 0;


  constructor() { }

  ngOnInit() {
    this.form = new FormGroup({});
  }

  submit() {
    console.log("ЕЕЕЕЕ");
  }

  onInput(event?: any) {
    if (this.name != '' && this.surname != '' && this.telephone != '') {
      this.disabled = false;
    }
  }

  addPeople() {
    if (this.name.trim() && this.surname.trim() && this.telephone.trim()) {
      const People : People = {
        name: this.name,
        surname: this.surname,
        telephone: this.telephone,
      }
      this.disabled = false;
      this.onAdd.emit(People);
      this.name = this.surname = this.telephone = '';
      this.disabled = true;
    }
  }

}

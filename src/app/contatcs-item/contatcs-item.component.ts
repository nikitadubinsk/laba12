import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { People } from '../app.component';

@Component({
  selector: 'app-contatcs-item',
  templateUrl: './contatcs-item.component.html',
  styleUrls: ['./contatcs-item.component.css']
})
export class ContatcsItemComponent implements OnInit {

  @Input() p : People;
  @Output() onDelete: EventEmitter<People> = new EventEmitter<People>();
  @Output() onSave: EventEmitter<People> = new EventEmitter<People>();

  edit = false;
  name = "";
  surname = "";
  telephone = "";
  id = 0;
  disabled = false;


  constructor() { }

  ngOnInit() {
    this.name = this.p.name;
    this.surname = this.p.surname;
    this.telephone = this.p.telephone;
  }

  deletePeople(id) {
    this.onDelete.emit(this.p);
  }

  editPeople(p : People) {
    this.edit = true;
  }

  onInput(event?: any) {
    if (this.name != '' && this.surname != '' && this.telephone != '') {
      this.disabled = false;
    } else this.disabled = true;
  }

  savePeople(id) {
    if (this.name.trim() && this.surname.trim() && this.telephone.trim()) {
      const People : People = {
        name: this.name,
        surname: this.surname,
        telephone: this.telephone,
        id: id,
      }

      this.edit = false;
      this.onSave.emit(People);
    }
  }
}
